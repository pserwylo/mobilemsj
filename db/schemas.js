export const FORM_SCHEMA = 'Form';
export const FORM_SUBMISSION = 'FormSubmission';

export const Form = {
    name: FORM_SCHEMA,
    properties: {
        id: 'int',
        name: 'string',
    }
};

export const FormSubmission = {
    name: FORM_SUBMISSION,
    properties: {
        id: 'int',
        form: FORM_SCHEMA,
        content: 'string',
        isDraft: {type: 'bool', default: true},
        createdTimestamp: 'date'
    }
};

export const schemas = [
    Form,
    FormSubmission
];
