import React from 'react';
import {Text} from 'native-base';
import {eventDuration, formatDate, formatTime} from "../events";
import moment from "moment-timezone";

export const StartEndTimeAndDuration = (props) =>
    <Text style={{color: '#666'}}>
        {formatTime(props.event.startDate) + " to " + formatTime(props.event.endDate) + " (" + moment.duration(eventDuration(props.event)).humanize() + ")"}
    </Text>;