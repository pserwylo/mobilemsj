import {Icon} from "native-base";

import React from "react";

export const TickIcon = () =>
    <Icon name="checkmark" />