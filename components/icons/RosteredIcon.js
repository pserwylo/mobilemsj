import {Icon} from "native-base";

import React from "react";
import {Alert} from "react-native";

const explainIcon = () => Alert.alert("Rostered", "Events which are \"Rostered\" are those which you have committed to attending.");

export const RosteredIcon = (props) => <Icon style={{color: props.colour}} name="checkmark-circle" onPress={() => props.help ? explainIcon : null}/>;