import {Icon} from "native-base";

import React from "react";
import {Alert} from "react-native";

export const EventIcon = ({colour = null}) => <Icon style={{color: colour}} name='medkit' />;