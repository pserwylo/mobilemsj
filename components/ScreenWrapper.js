import React from 'react';
import {Container, StyleProvider, Content} from 'native-base';
import {ImageBackground, StatusBar} from 'react-native';

import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';

export const ScreenWrapper = ({backgroundImage, children}) => (
    <StyleProvider style={getTheme(material)}>
        <Container>
            <ImageBackground
                source={backgroundImage == null ? null : require('../images/rawpixel-760113-unsplash.small.jpg')}
                style={{width: '100%', height: '100%'}}>
                <Content>
                    {children}
                </Content>
            </ImageBackground>
        </Container>
    </StyleProvider>
);