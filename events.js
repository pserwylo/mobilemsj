import moment from 'moment-timezone';
import * as net from './net';
import $ from 'cheerio';

export const fetchEoiDetails = async (sessionId, pdn) => {
    const dom = await net.get(net.BASE_URL + `/event/roster.jsp?action=submiteoi&event_id=${pdn}`, sessionId);
    const options = dom.find("select[name='shift']").find('option');
    const shifts = options.map((i, option) => {
        const label = $(option).text().trim();
        const value = $(option).val();
        return { label, value };
    }).get();

    return { pdn, shifts };
};

export const submitEoi = async (sessionId, pdn, shift) => {
    const dom = await net.post(net.BASE_URL + `/event/roster.jsp?action=submiteoi&event_id=${pdn}`, sessionId, { shift });

    const body = dom.find('#column_full');

    const submissionSuccessful =
        body.find('h1').text().trim() === 'Rostering' &&
        body.text().includes('Your EOI has now been submitted.');

    if (!submissionSuccessful) {
        console.log("Expected to find a header with 'Rostering', and the text 'Your EOI has now been submitted.'. However we were unable to find these.");
        console.log(body.find('h1').text().trim());
        console.log(body.text());
        console.log(dom.html());
        throw new Error("EOI submission failed.");
    }

    return true;
};

/**
 * MSJ represents dates in the format '21-Oct-2018 15:00'.
 */
const parseEventDate = (dateString) => {
    return moment.tz(dateString, 'DD-MMM-YYYY HH:mm', 'Australia/Melbourne').toDate()
};

/**
 * Take a jsdom "table" element, and turn each row into an object, indexed by the labels in the "th" cells.
 */
const parseTable = (dom) => {

    const headers = dom.find("thead th").map((i, th) => $(th).text()).get();

    const values = dom.find("tbody tr").map((i, tr) => {

        const cells = $(tr).find('td').map((i, td) => $(td).text().trim().replace(/\s\s+/g, ' '));

        const rowData = {};
        for (let i = 0; i < cells.length; i ++) {
            rowData[headers[i]] = cells[i];
        }

        return rowData;

    }).get();

    // Some tables (such as "My Hours" include rows that represent subtotals of all rows above them.
    // Exclude these, because they don't have any particular relevance in most cases.
    return values.filter(event => Object.values(event).length === headers.length);
};

// TODO: Change "Status" to "Staffing" ("Pending", "Scheduled") and "Rostered" ("No EOI", "Pending approval", "Rejected", "Accepted").
// Right now it is a little crazy here.

const parseEventFromFutureEvents = (event) => ({
    pdn: event['PDN'],
    name: event['Event Name'],
    startDate: parseEventDate(event['Start Date']),
    endDate: parseEventDate(event['End Date']),
    status: event['Status'],
    venueName: event['Venue Name'],
    venueAddress: event['Venue Address'],
    region: event['Region'],
    rostered: event['Action'] === 'Submit EOI' ? null : event['Action'],
});

const parseEventFromMyRoster = (event) => ({
    pdn: event['PDN'],
    name: event['Event Name'],
    startDate: parseEventDate(event['Start Date']),
    endDate: parseEventDate(event['End Date']),
    status: event['Status'],
    venueName: event['Venue Name'],
    region: event['Region'],
    rostered: event['Status'] === 'Rostered',
});

const parseEventFromMyHours = (event) => ({
    pdn: event['PDN'],
    name: event['Event Name'],
    startDate: parseEventDate(event['Start Date']),
    endDate: parseEventDate(event['End Date']),
    venueName: event['Venue'],
    organiser: event['Organiser'],
    hours: parseFloat(event['Hours']),
});

const parseEventsTableFromDom = (dom, eventParser) => {
    const eventsTable = dom.find(".pgAidKits");
    const eventsDataFromTable = parseTable(eventsTable);
    return eventsDataFromTable.map(eventParser);
};

// For some reason MSJ capitalises 'STATE' but no other divisions.
const formatDivisionName = (divisionName) => divisionName === "STATE" ? "State" : divisionName;

// When sorting, local divisions go first, then region, then state.
const divisionNameToSortValue = (divisionName) => {
    if (divisionName === "State") {
        return 3;
    }

    if (isRegion(divisionName)) {
        return 2;
    }

    return 1;
};

const parseDivisionIdsFromDom = (dom) =>
    dom.find('select[name=division_id] option')
        .map(
            (i, option) => ({
                id: parseInt($(option).val()),
                name: formatDivisionName($(option).text().trim())
            })
        )
        .get()
        .sort((div1, div2) => divisionNameToSortValue(div1.name) - divisionNameToSortValue(div2.name));

const parseDom = (dom, eventParser) => ({
    availableDivisions: parseDivisionIdsFromDom(dom),
    events: parseEventsTableFromDom(dom, eventParser)
});

export const fetchEvents = async (sessionId) => {
    const dom = await net.get(net.BASE_URL + '/event/list.jsp', sessionId);
    return parseDom(dom, parseEventFromFutureEvents);
};

export const fetchEventsForDivision = async (sessionId, divisionId) => {
    const dom = await net.post(
        net.BASE_URL + '/event/list.jsp?action=init&sort=e_startdate&sort_type=desc&pg_number=1',
        sessionId,
        { division_id: divisionId }
    );

    return parseDom(dom, parseEventFromFutureEvents);
};

export const fetchEventsFromMyRoster = async (sessionId) => {
    const dom = await net.get(net.BASE_URL + '/event/upcoming.jsp', sessionId);
    return parseDom(dom, parseEventFromMyRoster);
};

/**
 * Returns a Array of regions in the format {id: ID, name: NAME}.
 * This is quite wasteful though, because it pulls down the entire list of events just to ask for the divisions.
 * If you care about how many requests you make, then just fetch a list of events for your region using "fetchEvents",
 * and then the resulting data will contain 'availableDivisions'.
 */
export const fetchRegions = async (sessionId) => {
    const eventsData = await fetchEvents(sessionId);
    return eventsData.availableDivisions;
};

export const fetchMyHours = async (sessionId) => {
    const dom = await net.get(net.BASE_URL + '/event/hours.jsp', sessionId);
    return parseDom(dom, parseEventFromMyHours);
};

export const statusLabel = (status) => {
    switch(status) {
        case STATUS_ACTIVE: return 'Active';
        case STATUS_ROSTERED: return 'Rostered';
        case STATUS_SCHEDULED: return 'Scheduled';
        case STATUS_PENDING_STAFFING: return 'Pending';
        default: throw new Error(`Unknown status ${status}`);
    }
};

export const allEvents = (eventsByDivision) => Object.keys(eventsByDivision).reduce(
    (allEvents, key) => allEvents.concat(eventsByDivision[key]),
    []
);

export const formatDate = (date) => moment(date).format('ddd, Do MMM YYYY');

export const formatTime = (date) => moment(date).format('HHmm');

export const eventRelativeStartTime = (event) => moment(event.startDate).fromNow();

export const eventDuration = (event) => moment(event.endDate).diff(event.startDate);

export const STATUS_PENDING_STAFFING = 'PENDING_STAFFING';
export const STATUS_ACTIVE = 'ACTIVE';
export const STATUS_SCHEDULED = 'SCHEDULED';
export const STATUS_ROSTERED = 'Rostered';
export const STATUS_NOT_ACCEPTED = 'Not Accepted';
export const STATUS_EOI_PENDING = '???'; // TODO: Figure out how this works, and show it appropriately to the user.

export const isLocalDivision = (divisionName) => localDivisionNames.find(r => r === divisionName) != null;
export const isRegion = (divisionName) => regionNames.find(r => r === divisionName) != null;

// Scraped from https://intranet.stjohnvic.com.au/fas-directory/.
export const regionNames = ["Alpine","East","Gippsland","Grampians","Mallee","North","Otway","South","Urban","West"];
export const localDivisionNames = [
    "Altona",
    "Altona Youth",
    "Ballarat",
    "Banyule",
    "Barwon",
    "Barwon Youth",
    "Bayside",
    "Bayside Youth",
    "Bendigo",
    "BERT",
    "Brighton Grammar",
    "Brimbank",
    "Brimbank Youth",
    "Campaspe",
    "Cardinia",
    "Cardinia Youth",
    "Carey Grammar",
    "Casey",
    "Caulfield Grammar",
    "CBD",
    "Communications",
    "Darebin",
    "Daylesford",
    "Diamond Valley",
    "Djerriwarrh",
    "Djerriwarrh Youth",
    "Dr Edward Brentnall",
    "Dromana Secondary",
    "East Gippsland",
    "East Gippsland Youth",
    "Footscray",
    "Frankston",
    "Frankston Youth",
    "George Horne",
    "Glen Eira",
    "Goulburn Valley",
    "Greater Dandenong",
    "Greater Dandenong Youth",
    "Horsham",
    "Hume",
    "Kingston",
    "Kingston Youth",
    "Knox",
    "Knox Youth",
    "Latrobe",
    "Manningham",
    "Manningham Youth",
    "Maroondah",
    "Maroondah Youth",
    "Maryborough",
    "Maryborough Youth",
    "Mildura",
    "Monash",
    "Monash Youth",
    "Moonee Valley",
    "Moonee Valley Youth",
    "Moreland",
    "Mount Alexander",
    "Pascoe Vale Girls",
    "Phoenix",
    "Port Phillip",
    "Portland",
    "St Monica's",
    "Stonnington",
    "Sunbury Combined",
    "Sunbury Youth",
    "Warrnambool",
    "Wellington",
    "Whitehorse",
    "Whitehorse Youth",
    "Wodonga",
    "Wodonga Youth",
    "Wyndham",
    "Wyndham Youth",
    "Yarra",
    "Yarra Ranges",
    "Yarra Youth"
];