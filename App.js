/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, StatusBar, AsyncStorage} from 'react-native';
import {fetchEventsForDivision, fetchEventsFromMyRoster, fetchMyHours, fetchRegions} from './events';
import {login} from './auth';
import {ErrorScreen} from "./components/ErrorScreen";
import {AuthScreen} from "./components/AuthScreen";
import {MainScreen} from "./components/MainScreen";
import {SplashScreen} from "./components/SplashScreen";
import {EventsListScreen} from "./components/EventsListScreen";
import {LoadingScreen} from "./components/LoadingScreen";

class HomeScreen extends Component {

    state = {
        screen: 'splash',

        isLoading: true,
        loadingMessage: null,

        sessionId: null,

        // Store both of these so that if the session expires we can re-login.
        username: null,
        password: null,

        divisions: [],
        eventsByDivision: {},
        myUpcomingEvents: [],
        myPastEvents: [],

        // To pass to the events list screen as props.
        eventListScreen: {
            events: [],
            division: null,
            selectedStatus: null,
        },

        splashScreen: {
            isLoading: false,
            error: null,
        },
    };

    async componentWillMount() {
        console.log("Starting MSJ app");
        await this.setup();
    }

    async setup() {
        const username = await AsyncStorage.getItem('username');

        if (username == null) {
            this.setState({
                screen: 'splash',
                isLoading: false,
                loadingMessage: null,
            });
        } else {
            await this.initializeSessionAndData();
        }
    }

    async populateFromCache() {
        const divisionsJson = await AsyncStorage.getItem('divisions');
        if (divisionsJson === null) {
            return false;
        }

        const divisions = JSON.parse(divisionsJson);

        const eventsByDivisionJson = await AsyncStorage.getItem('eventsByDivision');
        if (eventsByDivisionJson === null) {
            return false;
        }

        const myUpcomingEventsJson = await AsyncStorage.getItem('myUpcomingEvents');
        if (myUpcomingEventsJson === null) {
            return false;
        }

        const myPastEventsJson = await AsyncStorage.getItem('myPastEvents');
        if (myPastEventsJson === null) {
            return false;
        }

        const eventsByDivision = JSON.parse(eventsByDivisionJson);
        const myUpcomingEvents = JSON.parse(myUpcomingEventsJson);
        const myPastEvents = JSON.parse(myPastEventsJson);

        this.setState({
            divisions,
            eventsByDivision,
            myUpcomingEvents,
            myPastEvents,
        });

        return true;
    }

    async cacheEvents() {
        await AsyncStorage.multiSet([
            ['lastCacheUpdate', new Date().toJSON()],
            ['divisions', JSON.stringify(this.state.divisions)],
            ['eventsByDivision', JSON.stringify(this.state.eventsByDivision)],
            ['myUpcomingEvents', JSON.stringify(this.state.myUpcomingEvents)],
            ['myPastEvents', JSON.stringify(this.state.myPastEvents)],
        ]);
    }

    async initializeSessionAndData(useCache = true) {

        console.log("Initializing auth and event data");

        this.setState({
            isLoading: true,
            loadingMessage: "Fetching list of events from MSJ...",
            error: false,
        });

        const username = await AsyncStorage.getItem('username');
        const password = await AsyncStorage.getItem('password');
        let sessionId = await AsyncStorage.getItem('sessionId');

        this.setState({
            username, password
        });

        if (useCache) {
            if (await this.isCacheStale()) {
                console.log("Not using cache, it is too old");
            } else {
                console.log("Loading event data from cache");

                if (await this.populateFromCache()) {

                    const numberFutureEvents = Object.keys(this.state.eventsByDivision).reduce(
                        (total, division) => this.state.eventsByDivision[division].length + total, 0
                    );

                    console.log(`Successfully loaded ${this.state.divisions.length} divisions, ${numberFutureEvents} future events, ${this.state.myUpcomingEvents.length} rostered events, and ${this.state.myPastEvents.length} of my past events from cache.`);

                    this.setState({
                        isLoading: false,
                        loadingMessage: null,
                        screen: 'main',
                    });

                    return;
                }

                console.log("No data in cache");
            }
        }

        if (username === null) {

            console.log("No username, will display auth form.");

            this.setState({
                isLoading: false,
                loadingMessage: null,
                screen: 'splash',
            });

            return;
        }

        // Hopefully we can reuse the existing session ID, and don't need to log in again.
        if (sessionId !== null) {

            console.log(`Existing session ID (${sessionId}) found, will try to load new data from the internet.`);

            try {
                await this.initializeData(sessionId);
                this.setState({
                    isLoading: false,
                    loadingMessage: null,
                    screen: 'main',
                });
                return;
            } catch (error) {
                if (error.name === "AuthError") {
                    // ... however if we failed to log in, then just continue on and we'll ask for a new session ID below.
                    await AsyncStorage.removeItem('sessionId');
                } else {
                    // ... if we get something else going wrong, we need to stop.
                    this.setState({
                        isLoading: false,
                        loadingMessage: null,
                        error: error.message,
                        screen: "error",
                    });
                    return;
                }
            }
        }

        try {
            console.log(`Logging in as ${username} to obtain new session ID.`);
            sessionId = await login(username, password);

            console.log(`Session ID (${sessionId}) obtained.`);
            await AsyncStorage.setItem('sessionId', sessionId);
        } catch (e) {
            this.setState({
                isLoading: false,
                loadingMessage: null,
                screen: 'error',
                error: 'An error occurred while logging in. Please check your network connection and username/password.',
            })
        }

        try {
            await this.initializeData(sessionId);
            this.setState({
                isLoading: false,
                loadingMessage: null,
                screen: 'main',
            });
        } catch (error) {
            const errorMessage = error.name === 'AuthError'
                ? 'Invalid username password. Please check your login details.'
                : 'An error occurred. Please check your network connection and username/password.';

            this.setState({
                isLoading: false,
                loadingMessage: null,
                screen: 'error',
                error: errorMessage,
            });
        }

    }

    async isCacheStale() {
        const lastCacheUpdateJson = await AsyncStorage.getItem('lastCacheUpdate');
        if (lastCacheUpdateJson == null) {
            return true;
        }

        const lastCacheUpdate = new Date(lastCacheUpdateJson);
        const minsSinceUpdate = (Date.now() - lastCacheUpdate.getTime()) / 1000 / 60;
        return minsSinceUpdate > 60;
    }

    async initializeData(sessionId) {

        const divisions = await fetchRegions(sessionId);

        const eventsByDivision = {};
        for (let i = 0; i < divisions.length; i ++) {
            const division = divisions[i];
            const eventsData = await fetchEventsForDivision(sessionId, division.id);
            eventsByDivision[division.id] = eventsData.events;
        }

        const myUpcomingEventsData = await fetchEventsFromMyRoster(sessionId);
        const myUpcomingEvents = myUpcomingEventsData.events;

        const myPastEventsData = await fetchMyHours(sessionId);
        const myPastEvents = myPastEventsData.events;

        this.setState({
            divisions,
            eventsByDivision,
            myUpcomingEvents,
            myPastEvents,
        }, () => this.cacheEvents());

    }

    async attemptLoginFromSplashScreen(username, password) {
        try {

            this.setState({
                splashScreen: {
                    isLoading: true,
                    error: null,
                },
            });

            await this.attemptLogin(username, password);

        } catch (error) {

            console.log("Error logging in from splash screen.", error);

            this.setState({
                splashScreen: {
                    isLoading: false,
                    error: 'Could not log you into MSJ. Please check your username and password.',
                }
            });

        }
    }

    async updateCredentials(username, password) {
        try {

            this.setState({
                isLoading: true,
                loadingMessage: "Fetching list of events from MSJ...",
            });

            await this.attemptLogin(username, password);

        } catch (error) {

            console.log("Error updating credentials.", error);

            this.setState({
                isLoading: false,
                loadingMessage: null,
                screen: 'error',
                error: 'Could not log you into MSJ. Please check your username and password.',
            });

        }
    }

    async attemptLogin(username, password, onError) {
        await AsyncStorage.setItem('username', username);
        const sessionId = await login(username, password);

        await this.initializeData(sessionId);

        // Only store this info after we have successfully initialized data, which meant we were
        // able to log in correctly.
        AsyncStorage.setItem('password', password);
        AsyncStorage.setItem('sessionId', sessionId);

        this.setState({
            username,
            password,
            screen: 'main',
            sessionId,
            isLoading: false,
            loadingMessage: null,
        });
    };

    showEventList(division, status = null) {
        this.setState({
            screen: 'eventList',
            eventListScreen: {
                events: this.state.eventsByDivision[division.id],
                division,
                selectedStatus: status,
            }
        });
    }

    render() {
        if (this.state.isLoading) {
            return <LoadingScreen loadingMessage={this.state.loadingMessage} />;
        }

        switch(this.state.screen) {
            case 'splash':
                return (
                    <SplashScreen
                        {...this.state.splashScreen}
                        onAttemptLogin={(username, password) => this.attemptLoginFromSplashScreen(username, password)}/>
                );
            case 'error':
                return (
                    <ErrorScreen
                        error={this.state.error}
                        onBack={() => this.setState({screen: 'main'})}
                    />
                );

            case 'main':
                return (
                    <MainScreen
                        {...this.state}
                        onRefresh={() => this.initializeSessionAndData(false)}
                        onSettings={() => this.setState({screen: 'auth'})}
                        onViewEventList={(division, status = null) => this.showEventList(division, status)}
                    />
                );

            case 'eventList':
                return (
                    <EventsListScreen
                        {...this.state.eventListScreen}
                        onBack={() => this.setState({screen: 'main'})}
                        onRefresh={() => this.initializeSessionAndData(false)}
                        onEventSelected={() => {}}/>
                );

            case 'auth':
                return (
                    <AuthScreen {...this.state}
                        onBack={() => this.setState({screen: 'main'})}
                        onCredentialsChanged={(username, password) => this.updateCredentials(username, password)}
                    />
                );

            default:
                throw new Error(`Unknown screen: "${this.state.screen}"`);
        }
    }

}

export default class App extends React.Component {
    render() {
        return (
            <HomeScreen />
        );
    }
}
