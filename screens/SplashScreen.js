import React from 'react';
import {StatusBar, StyleSheet, ImageBackground, View} from 'react-native';
import {Spinner, Button, Content, Form, Input, Item, Label, Text} from "native-base";
import {ScreenWrapper} from "../components/ScreenWrapper";
import {EventIcon} from "../components/icons/EventIcon";
import * as colours from "../components/colours";
import {styles} from "../components/styles";

export class SplashScreen extends React.Component {

    static navigationOptions = {
        title: 'Mobile MSJ',
        headerLeft: <View style={{marginLeft: 24}}><EventIcon colour='white' /></View>,
    };

    constructor(props) {
        super(props);

        if (props.sessionId) {
            console.log("Already logged in, skipping splash screen");
            this.props.navigation.replace('Main');
        }

        this.state = {
            username: props.username ? props.username : "",
            password: "",
        };
    }

    async login() {
        await this.props.onAttemptLogin(this.state.username, this.state.password);
        this.props.navigation.navigate('Main');
    }

    render() {
        // The constructor will have navigated away from here, but make sure not to actually try to flash up some
        // part of the UI if we have a session ID (and hence will be navigating away immediately).
        return (
            <ScreenWrapper backgroundImage>
                {this.props.sessionId ? null : this.innerContent()}
            </ScreenWrapper>
        );
    }

    innerContent() {
        return (
            <Content padder>
                <View style={styles.backgroundOverlay}>
                    {this.props.isLoading ? <Spinner /> :
                        <View>
                            <Text style={localStyles.title}>Mobile MSJ</Text>
                            <Text style={localStyles.titleSmall}>(an unofficial app)</Text>
                            <Form style={localStyles.form}>
                                <Item stackedLabel>
                                    <Label>Username</Label>
                                    <Input onChangeText={(text) => this.setState({username: text})} value={this.state.username} />
                                </Item>
                                <Item stackedLabel>
                                    <Label>Password</Label>
                                    <Input secureTextEntry onChangeText={(text) => this.setState({password: text})} value={this.state.password} />
                                </Item>
                                {this.props.error == null ? null : <Text style={styles.error}>{this.props.error}</Text>}
                                <Button
                                    style={localStyles.button}
                                    disabled={this.state.username.length === 0 || this.state.password.length === 0}
                                    onPress={() => this.login()}>
                                    <Text>Login</Text>
                                </Button>
                            </Form>
                        </View>}
                </View>
            </Content>
        );
    }
}

const localStyles = StyleSheet.create({
    title: {
        fontSize: 24,
        color: colours.theme,
        paddingBottom: 8,
        textAlign: "center",
    },
    titleSmall: {
        fontSize: 16,
        color: colours.theme,
        textAlign: "center",
    },
    button: {
        marginTop: 16,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    form: {
        marginTop: 10,
    }
});