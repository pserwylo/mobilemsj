import React from "react";

import {Button, Content, Text} from "native-base";
import {View, StyleSheet, Linking} from 'react-native';

import {ScreenWrapper} from "../components/ScreenWrapper";
import {styles} from '../components/styles';
import * as events from '../events';
import {formatDate, formatTime, eventRelativeStartTime} from "../events";

export class EventScreen extends React.Component {

    static navigationOptions = {
        title: 'Event Details',
    };

    constructor(props) {
        super(props);

        this.state = {
            event: props.navigation.getParam('event'),
        }
    }

    renderEvent(event) {
        if (event.rostered) {
            return <RosteredEvent event={event} />;
        }

        switch(event.status) {
            case events.STATUS_SCHEDULED:
                return <ScheduledEvent event={event} onEoi={this.props.onEoi} />;

            case events.STATUS_NOT_ACCEPTED:
            case events.STATUS_PENDING_STAFFING:
            case events.STATUS_ACTIVE:
            case events.STATUS_EOI_PENDING:
            default:
                return <NormalEvent event={event} />;
        }
    }

    render() {
        return (
            <ScreenWrapper backgroundImage>
                <Content>
                    <View style={styles.backgroundOverlay}>
                        {this.renderEvent(this.state.event)}
                    </View>
                </Content>
            </ScreenWrapper>
        );
    }

}

/**
 * We don't get much info from MSJ, really just a name and date.
 * However, we can probably point you to Google to get more details, which is what we all do anyway.
 * @param props
 * @constructor
 */
const EventHeader = ({event}) => {

    const google = () => {
        Linking.openURL(`https://www.google.com.au/search?q=${event.name}`);
    };

    return (
        <View>
            <Text style={localStyles.eventNameText}>
                {event.name}
            </Text>

            <Text style={localStyles.eventDateText}>
                {formatDate(event.startDate)}, {formatTime(event.startDate)} to {formatTime(event.endDate)} ({eventRelativeStartTime(event)})
            </Text>

            <Button transparent onPress={() => google()} style={eventHeaderStyles.googleButton}>
                <Text>Google event</Text>
            </Button>
        </View>
    )

};

/**
 * You will be going to this event, so you need to know as many details as possible.
 * If you want to un-EOI, you can't do it from here, so we need to point you to your Ops person.
 */
const RosteredEvent = ({event}) =>
    <View>
        <EventHeader {...{event}} />
        <View style={[infoBoxStyles.box, rosteredStyles.box]}>
            <Text style={rosteredStyles.headerText}>
                Event roster full
            </Text>
            <Text style={rosteredStyles.bodyText}>
                This event currently has enough start.
                You can EOI for this event, but you will likely only be put on reserve.
            </Text>
        </View>
    </View>;

/**
 * You've submitted an EOI, but have not yet been approved.
 */
const PendingEvent = ({event}) =>
    <View>
        <EventHeader {...{event}} />
    </View>;

/**
 * You can EOI for this, but you will probably end up on reserve as it is already full.
 */
const ScheduledEvent = ({event, onEoi}) =>
    <View>
        <EventHeader {...{event}} />
        <View style={[infoBoxStyles.box, scheduledStyles.box]}>
            <Text style={scheduledStyles.headerText}>
                Event roster full
            </Text>
            <Text style={scheduledStyles.bodyText}>
                This event currently has enough start.
                You can EOI for this event, but you will likely only be put on reserve.
            </Text>
        </View>
    </View>;

/**
 * You can EOI for this, but you will probably end up on reserve as it is already full.
 */
const NormalEvent = ({event, onEoi}) =>
    <View>
        <EventHeader {...{event}} />
    </View>;

const rosteredStyles = StyleSheet.create({
    headerText: {
        fontWeight: "700",
        color: "#056500",
        fontSize: 14,
    },
    bodyText: {
        color: "#056500",
        fontSize: 14,
    },
    box: {
        borderColor: "#3d8736",
        backgroundColor: "#c7ffb7",
    },
});

const eventHeaderStyles = StyleSheet.create({
    googleButton: {
        fontSize: 14,
    },
});

const scheduledStyles = StyleSheet.create({
    headerText: {
        fontWeight: "700",
        color: "#655000",
        fontSize: 14,
    },
    bodyText: {
        color: "#655000",
        fontSize: 14,
    },
    box: {
        borderColor: "#ffcf5d",
        backgroundColor: "#fff5b9",
    },
});

const infoBoxStyles = StyleSheet.create({
    box: {
        borderStyle: "solid",
        borderWidth: 1,
        padding: 8,
        paddingLeft: 16,
        paddingRight: 16,
        marginTop: 8,
    },
});

const localStyles = StyleSheet.create({
    eventNameText: {
        fontSize: 16,
        fontWeight: "700",
    },
    eventDateText: {
        fontSize: 14,
        fontWeight: "200",
        color: "#666",
        marginTop: 4,
        marginBottom: 8,
    },
});